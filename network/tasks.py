#Create your tasks here

from __future__ import absolute_import, unicode_literals
from celery import shared_task
from digi.xbee.devices import XBeeDevice
from digi.xbee.exception import InvalidPacketException
import serial, time


# TODO: Replace with the serial port where your local module is connected to.
PORT = "/dev/tty.usbserial-DN02PIIG"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

@shared_task
def receive_data(request):
    # attempt to fix operating mode issue by forcing bypass mode
    ser = serial.Serial(PORT, BAUD_RATE)
    ser.write(bytes("\n\n", 'utf-8'))
    time.sleep(0.1)
    ser.write(bytes("B", 'utf-8'))
    time.sleep(0.1)
    # end
    device = XBeeDevice(PORT, BAUD_RATE)

    try:
        #device = XBeeDevice(PORT, BAUD_RATE)
        x_1 = float('-inf')
        y_1 = float('-inf')
        z_1 = float('-inf')
        x_2 = float('-inf')
        y_2 = float('-inf')
        z_2 = float('-inf')
        rawX = []
        rawY = []
        filX = []
        filY = []
        avgX = []
        avgY = []
        testX = []
        testY = []
        custX = []
        custY = []
        ct = 0

        device.open()

        device.flush_queues()

        print("Waiting for data...\n")

        """
        def makeFig():
            plt.axis([-3.5, 10.5, -2.5, 2.0])
            plt.plot(filX, filY, 'g^')
            plt.plot(testX, testY, 'b^')
            plt.plot(avgX, avgY, 'ro')
            plt.plot(custX, custY, 'y^')
            plt.plot([x1, x2, x3], [y1, y2, y3], 'bs')
        """
        while True:
            xbee_message = device.read_data()
            if xbee_message is not None:
                addr = str(xbee_message.remote_device.get_64bit_addr())
                inputs = xbee_message.data.decode().split(',')

                # Data is organized like this:
                # input[0] is last 2 digits of mac addr
                # input[1] is raw distance
                # input[2] is averaged distance
                # input[3] is rssi
                # input[4] is rssi at 1m, as used with the distance formula
                # Feel free to do whatever

                if inputs[0] == 'cc':
                    if addr[-3:] == '242':
                        x_1 = float(inputs[1])
                        x_2 = float(inputs[2])
                    elif addr[-3:] == '223':
                        y_1 = float(inputs[1])
                        y_2 = float(inputs[2])
                    elif addr[-3:] == '948':
                        z_1 = float(inputs[1])
                        z_2 = float(inputs[2])

                    # TODO: make it support multiple, as in more than 3 zigbees
                    # In theory: map each beacon to 3 zigbee device infos, update the buffer
                    if x_1 != float('-inf') and y_1 != float('-inf') and z_1 != float('-inf'):
                        coord = triangulate2(x_1, y_1, z_1)
                        coord1 = triangulate2(x_2, y_2, z_2)
                        coord2 = biangulate(x_2, y_2)
                        print("Located at: " + str(coord[0]) + " , " + str(coord[1]))
                        print("Using averages: " + str(coord1[0]) + " , " + str(coord1[1]))
                        print("Using test: " + str(coord2[0]) + " , " + str(coord2[1]))
                        print("Using cust " + str(coord1[0]) + " , " + str(coord2[1]))
                        if ct > 40:
                            filX.pop(0)
                            filY.pop(0)
                            avgX.pop(0)
                            avgY.pop(0)
                            testX.pop(0)
                            testY.pop(0)
                            custX.pop(0)
                            custY.pop(0)

                        filX.append(coord[0])
                        filY.append(coord[1])
                        avgX.append(coord1[0])
                        avgY.append(coord1[1])
                        testX.append(coord2[0])
                        testY.append(coord2[1])
                        custX.append(coord1[0])
                        custY.append(coord2[1])
                        # drawnow(makeFig)
                        # plt.pause(0.00001)
                        ct += 1


    except InvalidPacketException:
        print("Invalid Checksum??")
        pass

    except KeyboardInterrupt:
        if device is not None and device.is_open():
            device.close()
    finally:
        if device is not None and device.is_open():
            device.close()
