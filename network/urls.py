from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.beacon_list, name='beacon_list'),
    path('routers/', views.router_list, name='router_list'),
    # url(r'^beacon/(?P<pk>\d+)/$', views.beacon_detail, name='beacon_detail'),

]
