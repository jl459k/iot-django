from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Beacon)
admin.site.register(BeaconData)
admin.site.register(Router)
