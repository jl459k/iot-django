from django.db import models

# Create your models here.


class Router(models.Model):
    node_zbMacAddress = models.CharField(max_length=30, primary_key=True)
    node_name = models.CharField(max_length=30)
    node_location_x = models.FloatField(null=True)
    node_location_y = models.FloatField(null=True)


class Beacon(models.Model):
    beacon_macAddress = models.CharField(max_length=12, primary_key=True)
    beacon_name = models.CharField(max_length=20)
    beacon_type = models.CharField(max_length=30)
    beacon_avg_location_x = models.FloatField(null=True)
    beacon_avg_location_y = models.FloatField(null=True)
    beacon_raw_location_x = models.FloatField(null=True)
    beacon_raw_location_y = models.FloatField(null=True)
    beacon_biangulate_location_x = models.FloatField(null=True)
    beacon_biangulate_location_y = models.FloatField(null=True)
    beacon_router1 = models.ForeignKey(Router, on_delete=models.SET_NULL, null=True, related_name='router1')
    beacon_router2 = models.ForeignKey(Router, on_delete=models.SET_NULL, null=True, related_name='router2')
    beacon_router3 = models.ForeignKey(Router, on_delete=models.SET_NULL, null=True, related_name='router3')


class BeaconData(models.Model):
    beacon = models.ForeignKey(Beacon, on_delete=models.CASCADE, null=True)
    router = models.ForeignKey(Router, on_delete=models.CASCADE, null=True)
    rssi_val = models.FloatField(null=True)
    raw_distance = models.FloatField(null=True)
    avg_distance = models.FloatField(null=True)
    date_time = models.DateTimeField(null=True)
