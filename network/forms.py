from django import forms
from .models import Beacon

class BeaconForm(forms.ModelForm):

    class Meta:
        model = Beacon
        fields = ('Beacon_MAC_Address', 'Beacon_name', 'Beacon_type', 'Beacon_location_X', 'Beacon_location_Y',)

