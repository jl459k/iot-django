from django.shortcuts import render
from .models import Beacon, BeaconData, Router

# Create your views here.


def beacon_list(request):
    beacons = Beacon.objects.order_by('beacon_name')
    return render(request, 'beacon_list.html', {'beacons': beacons})


def beacondata_list(request):
    beacondata = BeaconData.objects.order_by('-date_time')
    return render(request, 'beacon_data.html', {'beacondata': beacondata})


def router_list(request):
    router = Router.objects.order_by('node_name')
    return render(request, 'router_list.html', {'routers': router})


def get_more_tables(request):
    increment = int(request.GET['increment'])
    increment_to = increment + 10
    beacon_data = BeaconData.objects.order_by('-date_time')[increment:increment_to]
    return render(request, 'get_more_tables.html', {'beacon_data': beacon_data})

