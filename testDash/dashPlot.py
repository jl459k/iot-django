import dash
import time
import fcntl
import numpy as np
import sqlite3
from dash.dependencies import Output, Event
import dash_core_components as dcc
import dash_html_components as html

import plotly
import random
import plotly.graph_objs as go
from collections import deque

dataDict = {}
finalData = deque


def average(l):
    return np.mean(np.array(l))


items = ['fil', 'avg', 'test']
for item in items:
    dataDict[item] = (deque(maxlen=40), deque(maxlen=40))

checkpoint = None

app = dash.Dash(__name__)
app.layout = html.Div(
    [
        dcc.Graph(id='live-graph', animate=True),
        dcc.Interval(
            id='graph-update',
            interval=1 * 500
        ),
    ]
)


@app.callback(Output('live-graph', 'figure'),
              events=[Event('graph-update', 'interval')])
def update_graph_scatter():
    for item in items:
        with open('../pText/' + item + 'X' + '.csv', 'r') as f:
            with open('../pText/' + item + 'Y' + '.csv', 'r') as g:
                fcntl.flock(f, fcntl.LOCK_EX)
                lineF = f.readlines()
                fcntl.flock(f, fcntl.LOCK_UN)
                fcntl.flock(f, fcntl.LOCK_EX)
                lineG = g.readlines()
                fcntl.flock(f, fcntl.LOCK_UN)
                # This is probably awful
                a = lineF[-10:-9][0].split(',')
                b = lineG[-10:-9][0].split(',')
                dataDict[item][0].append(float(a[1]))
                dataDict[item][1].append(float(b[1]))

    final = [go.Scatter(
        x=[0.0, 9.2, 2.432],
        y=[0.0, 0.0, 1.2192],
        name='Node',
        mode='markers'
    )]

    for item in items:
        final.append(go.Scatter(
            x=list(dataDict[item][0]),
            y=list(dataDict[item][1]),
            name=item,
            mode='markers'
        ))
    print(list(dataDict['avg'][0]))
    final.append(go.Scatter(
        x=[average(list(dataDict['avg'][0]))],
        y=[average(list(dataDict['test'][1]))],
        name='cust',
        mode='markers',
        marker=dict(
            size=10,
            color='rgba(10, 182, 193, .9)',
            line=dict(
                width=2,
            )
        )
    )
    )

    return {'data': final, 'layout' : go.Layout(xaxis=dict(range=[-5,12]),
                                                yaxis=dict(range=[-10,10]),)}




if __name__ == '__main__':
    app.run_server(debug=True)
