import dash
import time
import fcntl
import numpy as np
import sqlite3
from dash.dependencies import Output, Event
import dash_core_components as dcc
import dash_html_components as html

import plotly
import random
import plotly.graph_objs as go
from collections import deque

dataDict = {}
plotDict = {}
locations = {}
locations['rad1'] = (0.0, 0.0)
locations['rad2'] = (5.0, 0.0)
locations['rad3'] = (2.432, 1.2192)

finalData = deque
doScatter = True

items = ['rad1', 'rad2', 'rad3']
for item in items:
    dataDict[item] = deque(maxlen=40)

plots = ['fil', 'avg', 'test']
for plot in plots:
    plotDict[plot] = (deque(maxlen=40), deque(maxlen=40))

checkpoint = None

app = dash.Dash(__name__)
app.layout = html.Div(
    [
        dcc.Graph(id='live-graph', animate=False),
        dcc.Interval(
            id='graph-update',
            interval=1 * 500
        ),
    ]
)

def average(l):
    return np.mean(np.array(l))

def my_circle(center, radius, n_points=75):
    t=np.linspace(0, 1, n_points)
    x=center[0]+radius*np.cos(2*np.pi*t)
    y=center[1]+radius*np.sin(2*np.pi*t)
    return x, y 


@app.callback(Output('live-graph', 'figure'),
              events=[Event('graph-update', 'interval')])

def update_graph_scatter():
    for item in items:
        with open('../pText/' + item + '.csv', 'r') as f:
            fcntl.flock(f, fcntl.LOCK_EX)
            lineF = f.readlines()
            fcntl.flock(f, fcntl.LOCK_UN)
            # This is probably awful
            a = lineF[-10:-9][0].split(',')
            dataDict[item].append(float(a[1]))

    paths = []
    for item in items:
        # some fucking magic
        x, y = my_circle(locations[item], dataDict[item][-1])
        path = 'M ' + str(x[0]) + ',' + str(y[1])
        for k in range(1, x.shape[0]):
             path += ' L ' + str(x[k]) + ',' + str(y[k])
        path += ' Z'
        # end
        paths.append(path)

    finalShape = [
        # unfilled circle, also bad code
        dict(type = 'path',
        layer = 'below',
        path = paths[0],
        line = dict(color = 'rgb(160, 44, 101)')
        ),
        dict(type = 'path',
        layer = 'below',
        path = paths[1],
        line = dict(color = 'rgb(44, 160, 101)')
        ),
        dict(type = 'path',
        layer = 'below',
        path = paths[2],
        line = dict(color = 'rgb(44, 101, 160)')
        )
    ]

    with open('../config/walls.csv', 'r') as h:
        lineF = h.readlines()
        for line in lineF:
            lines = line.rstrip().split(',')
            finalShape.append({
                'type': 'line',
                'xref': 'x',
                'yref': 'y',
                'x0': float(lines[0]),
                'y0': float(lines[1]),
                'x1': float(lines[2]),
                'y1': float(lines[3]),
                'line': {
                    'color': 'rgb(190, 171, 26)',
                    'width': 3,
                },
            })
   


    final = [go.Scatter(
        x=[0.0, 5.0, 2.432],
        y=[0.0, 0.0, 1.2192],
        name='Node',
        mode='markers'
    )]

    # all the scatter plot logic goes within these comments
        

    if doScatter:
        for plot in plots:
            if plot == 'avg':
                with open('../pText/' + plot + 'X' + '.csv', 'r') as f:
                    with open('../pText/' + plot + 'Y' + '.csv', 'r') as g:
                        fcntl.flock(f, fcntl.LOCK_EX)
                        lineF = f.readlines()
                        fcntl.flock(f, fcntl.LOCK_UN)
                        fcntl.flock(f, fcntl.LOCK_EX)
                        lineG = g.readlines()
                        fcntl.flock(f, fcntl.LOCK_UN)
                        # This is probably awful
                        a = lineF[-10:-9][0].split(',')
                        b = lineG[-10:-9][0].split(',')
                        plotDict[plot][0].append(float(a[1]))
                        plotDict[plot][1].append(float(b[1]))
                        final.append(go.Scatter(
                            x = list(plotDict[plot][0]),
                            y = list(plotDict[plot][1]),
                            name = plot,
                            mode = 'markers'
                        ))
                        print(a[1].strip('\n') + ',' + b[1])
    # end

    layout = {
    'xaxis': {
        'range': [-16, 16],
        'zeroline': False,
    },
    'yaxis': {
        'range': [-16, 16],
        'zeroline': False,
    },
    'width': 1200,
    'height': 1200,
    'shapes': finalShape
}

    return {'data': final, 'layout' : layout}




if __name__ == '__main__':
    app.run_server(debug=True)
