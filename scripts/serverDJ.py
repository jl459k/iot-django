# Copyright 2017, Digi International Inc.

import math
import numpy as np 
import time
import serial
import csv
import fcntl

from digi.xbee.devices import XBeeDevice
from digi.xbee.exception import InvalidPacketException


# TODO: Replace with the serial port where your local module is connected to. 
PORT = "/dev/tty.usbserial-DN02PIIG"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

# Config Hallway: (9.2, 0) (2.432, 1.2192), (0, 0) 

# Config that open space: (0,0) (3.05, -6.09), (10.05, 0)

# Actual measured RSSI: (242 -> -46.92) (223 -> -49) (948 -> -46) 

data = ['filX', 'filY', 'avgX', 'avgY', 'testX', 'testY', 'rad1', 'rad2', 'rad3']

averages = {}
zigbees = {}
dataD = {}
valsD = {}
dist = {}
avgS = {}

# read data from csv:
locList = []
with open('./config/node.csv', 'r') as h:
    lineF = h.readlines()
    for line in lineF:
        lines = line.rstrip().split(',')
        locList.append(lines)

# end


# these shouldn't be hardcoded here
x1 = 0
y1 = 0

x2 = 5.0
y2 = 0

x3 = 2.432
y3 = 1.2192

# end

d = x2

def average(l):
    return np.mean(np.array(l))

def triangulate(r1, r2, r3):
    x = (r1**2.0) - (r2**2.0) + d
    x = x / (2.0 * d)
    y = ((r1**2.0) - (r3**2.0) + x3**2.0 + y3**2.0) / (2*y3) - (x3/y3)*x
    return (x, y)

def triangulate2(r1, r2, r3):
    A = -2*x1 + 2*x2
    B = -2*y1 + 2*y2
    C = r1**2 - r2**2 - x1**2 + x2**2 - y1**2 + y2**2
    D = -2*x2 + 2*x3
    E = -2*y2 + 2*y3
    F = r2**2 - r3**2 - x2**2 + x3**2 - y2**2 + y3**2

    a = np.array([[A,B],[D,E]])
    b = np.array([C,F])

    solution = np.linalg.solve(a, b)
    return solution

def calculate(rssi, txPower):
    if rssi == 0:
        return -1.0
    val = txPower - rssi
    final = val / (10.0 * 2.0)
    distance = math.pow(10, final)
    return distance

def biangulate(r1, r2):
    x = (r1**2.0) - (r2**2.0) + d
    x = x / (2.0 * d)
    y = math.sqrt(abs((r1**2.0) - (x**2.0)))
    return (2.0*y, x)

def main():
    print(" +-------------------------------------------------+")
    print(" |            XBee iBeacon Tracking Demo           |")
    print(" +-------------------------------------------------+\n")

    # attempt to fix operating mode issue by forcing bypass mode
    ser = serial.Serial(PORT, BAUD_RATE)
    ser.write(bytes("\n\n", 'utf-8'))
    time.sleep(0.1)
    ser.write(bytes("B", 'utf-8'))
    time.sleep(0.1)
    # end

    try:
        device = XBeeDevice(PORT, BAUD_RATE)
        x_1 = y_1 = z_1 = x_2 = y_2 = z_2 = float('-inf')
        for item in data:
            dataD[item] = []

        ct = 0

        device.open()
        device.flush_queues()
        print("Waiting for data...\n")

        while True:
            xbee_message = device.read_data()
            if xbee_message is not None:
                addr = str(xbee_message.remote_device.get_64bit_addr())
                inputs = xbee_message.data.decode().split(',')

                # Data is organized like this:
                # input[0] is last 2 digits of mac addr
                # input[1] is raw distance
                # input[2] is averaged distance
                # input[3] is rssi 
                # input[4] is rssi at 1m, as used with the distance formula
                # Feel free to do whatever

                if inputs[0][-2:] == '27':
                    if addr[-3:] not in avgS:
                        dist[addr[-3:]] = []
                        avgS[addr[-3:]] = float('-inf')

                    if addr[-3:] == '242':
                        
                        x_1 = calculate(float(inputs[3]), -50.0)
                        dist[addr[-3:]].append(x_1)
                        avgS[addr[-3:]] = (average(dist[addr[-3:]][-10:]))
                        x_2 = avgS[addr[-3:]]
                        valsD['rad1'] = x_2

                        print("27 is: " + str(x_2) + " metres away from " + addr[-3:])

                    elif addr[-3:] == '223':
                        
                        y_1 = calculate(float(inputs[3]), -50.0)
                        dist[addr[-3:]].append(y_1)
                        avgS[addr[-3:]] = (average(dist[addr[-3:]][-10:]))
                        y_2 = avgS[addr[-3:]]
                        valsD['rad2'] = y_2

                        print("27 is: " + str(y_2) + " metres away from " + addr[-3:])

                    elif addr[-3:] == '948':
                        
                        z_1 = calculate(float(inputs[3]), -50.0)
                        dist[addr[-3:]].append(z_1)
                        avgS[addr[-3:]] = (average(dist[addr[-3:]][-10:]))
                        z_2 = avgS[addr[-3:]]
                        valsD['rad3'] = z_2

                        print("27 is: " + str(z_2) + " metres away from " + addr[-3:])

                    # TODO: make it support multiple, as in more than 3 zigbees
                    # In theory: map each beacon to 3 zigbee device infos, update the buffer

                    if x_1 != float('-inf') and y_1 != float('-inf') and z_1 != float('-inf') and len(dist[addr[-3:]]) >= 10:
                        # detection algo here!!!!!
                        if x_2 < 2.0 and y_2 < 2.0 and z_2 < 2.0:
                            print("DETECTED!!!")
                        # end
                        valsD['filX'], valsD['filY'] = triangulate2(x_1, y_1, z_1)
                        valsD['avgX'], valsD['avgY'] = triangulate2(x_2, y_2, z_2)
                        valsD['testX'], valsD['testY'] = biangulate(x_2, y_2)
                        

                        # csv updates
                        for item in data:
                            with open('./pText/' + item + '.csv', 'a') as f:
                                finalData = str(time.time()) + ',' + str(valsD[item]) + '\n'
                                fcntl.flock(f, fcntl.LOCK_EX)
                                f.write(finalData)
                                fcntl.flock(f, fcntl.LOCK_UN)
                        # end

                        if ct > 40:
                            for item in dataD:
                                dataD[item].pop(0)

                        for item in data:
                            dataD[item].append(valsD[item])

                        ct += 1

    except InvalidPacketException:
        print("Invalid Checksum??")
        pass

    except KeyboardInterrupt:
        if device is not None and device.is_open():
            device.close()
    finally:
        if device is not None and device.is_open():
            device.close()


if __name__ == '__main__':
    main()
