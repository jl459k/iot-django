# python-xbee Copyright 2017, Digi International Inc.

import time
from digi.xbee.devices import XBeeDevice
from digi.xbee.exception import InvalidPacketException
import serial
import numpy as np

# Data is organized like this:
# input[0] is last 2 digits of mac addr
# input[1] is raw distance
# input[2] is averaged distance
# input[3] is rssi 
# input[4] is rssi at 1m, as used with the distance formula
# Feel free to do whatever

# TODO: Replace with the serial port where your local module is connected to. 
PORT = "/dev/tty.usbserial-DN02PIIG"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

#MK2 at 250, MK3 at 242 MK2 is about (2, 0.533) MK3 is about (0,0)
averages = {}


def singulate(d):
    return

def biangulate(x,y):
    return

def average(l):
    return np.mean(np.array(l))

def main():
    try:
        print(" +-------------------------------------------------+")
        print(" |            XBee iBeacon Tracking Demo           |")
        print(" +-------------------------------------------------+\n")
        # attempt to fix operating mode issue by forcing bypass mode
        ser = serial.Serial(PORT, BAUD_RATE)
        ser.write(bytes("\n\n", 'utf-8'))
        time.sleep(0.1)
        ser.write(bytes("B", 'utf-8'))
        time.sleep(0.1)
        # end
        device = XBeeDevice(PORT, BAUD_RATE)

        
        device.open()

        def data_receive_callback(xbee_message):
            addr = str(xbee_message.remote_device.get_64bit_addr())
            a = xbee_message.data.decode().split(',')
            if addr not in averages:
                averages[addr] = [float(a[3])]
            else:
                averages[addr].append(float(a[3]))
            b = xbee_message.data.decode() + ',' + str(average(averages[addr][-100:]))
            print("From %s >> %s" % (addr, b))



        device.add_data_received_callback(data_receive_callback)

        print("Waiting for data...\n")

        input()

    except InvalidPacketException:
        print("Invalid Checksum??")
        pass
    finally:
        if device is not None and device.is_open():
            device.close()


if __name__ == '__main__':
    main()
