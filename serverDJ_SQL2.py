# Copyright 2017, Digi International Inc.

import math
import numpy as np
import time
import sqlite3
import datetime
import time
import serial
import csv
# import fcntl

# for regex
import re

from digi.xbee.devices import XBeeDevice
from digi.xbee.exception import InvalidPacketException

# TODO: Replace with the serial port where your local module is connected to.
PORT = "/dev/tty.usbserial-DN02PIIG"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

# Config Hallway: (9.2, 0) (2.432, 1.2192), (0, 0)

# Config that open space: (0,0) (3.05, -6.09), (10.05, 0)

data = ['filX', 'filY', 'avgX', 'avgY', 'testX', 'testY']

averages = {}
zigbees = {}
dataD = {}
valsD = {}

x1 = 0
y1 = 0
x2 = 9.2
y2 = 0
x3 = 2.432
y3 = 1.2192
d = 9.2


def triangulate(r1, r2, r3):
    x = (r1 ** 2.0) - (r2 ** 2.0) + d
    x = x / (2.0 * d)
    y = ((r1 ** 2.0) - (r3 ** 2.0) + x3 ** 2.0 + y3 ** 2.0) / (2 * y3) - (x3 / y3) * x
    return (x, y)


def triangulate2(r1, r2, r3):
    A = -2 * x1 + 2 * x2
    B = -2 * y1 + 2 * y2
    C = r1 ** 2 - r2 ** 2 - x1 ** 2 + x2 ** 2 - y1 ** 2 + y2 ** 2
    D = -2 * x2 + 2 * x3
    E = -2 * y2 + 2 * y3
    F = r2 ** 2 - r3 ** 2 - x2 ** 2 + x3 ** 2 - y2 ** 2 + y3 ** 2

    a = np.array([[A, B], [D, E]])
    b = np.array([C, F])

    solution = np.linalg.solve(a, b)
    # x = (C*E - F*B) / (E*A - B*D)
    # y = (C*D - A*F) / (B*D - A*E) + 15
    # return (x, y)
    solution[1] = solution[1] + 15
    return solution


def calculate(rssi, txPower):
    if rssi == 0:
        return -1.0
    val = txPower - rssi
    final = val / (10.0 * 2.0)
    distance = math.pow(10, final)
    return distance


def biangulate(r1, r2):
    x = (r1 ** 2.0) - (r2 ** 2.0) + d
    x = x / (2.0 * d)
    y = math.sqrt(abs((r1 ** 2.0) - (x ** 2.0)))
    return (2.0 * y, x)


def main():
    print(" +-------------------------------------------------+")
    print(" |            XBee iBeacon Tracking Demo           |")
    print(" +-------------------------------------------------+\n")

    # attempt to fix operating mode issue by forcing bypass mode
    ser = serial.Serial(PORT, BAUD_RATE)
    ser.write(bytes("\n\n", 'utf-8'))
    time.sleep(0.1)
    ser.write(bytes("B", 'utf-8'))
    time.sleep(0.1)
    # end

    device = XBeeDevice(PORT, BAUD_RATE)
    conn = sqlite3.connect('db.sqlite3', detect_types=sqlite3.PARSE_DECLTYPES)
    try:
        device = XBeeDevice(PORT, BAUD_RATE)
        x_1 = y_1 = z_1 = x_2 = y_2 = z_2 = float('-inf')
        for item in data:
            dataD[item] = []

        ct = 0

        device.open()
        device.flush_queues()
        print("Waiting for data...\n")

        c = conn.cursor()
        c.execute('''DELETE from network_beacondata''')
        c.execute('''DELETE from network_beacon''')
        while True:
            xbee_message = device.read_data()
            if xbee_message is not None:
                addr = str(xbee_message.remote_device.get_64bit_addr())
                inputs = xbee_message.data.decode().split(',')

                # Data is organized like this:
                # input[0] is last 2 digits of mac addr
                # input[1] is raw distance
                # input[2] is averaged distance
                # input[3] is rssi
                # input[4] is rssi at 1m, as used with the distance formula
                # Feel free to do whatever

                # check to see if router is in database
                c.execute('''SELECT node_zbMacAddress FROM network_router WHERE node_zbMacAddress = ?''',
                          [addr])
                data1 = c.fetchall()
                if len(data1) == 0:
                    c.execute('''INSERT INTO network_router(node_zbMacAddress, node_name) values (?, ?)''',
                              (addr, addr[-3:]))
                # inserts lines of data sent from beacon to router in beacondata table
                c.execute('''INSERT INTO network_beacondata(beacon_id, router_id, rssi_val, raw_distance, avg_distance,
                                                                        date_time) values (?, ?, ?, ?, ?, ?)''',
                          (inputs[0], addr, inputs[3], inputs[1], inputs[2], str(datetime.datetime.now())))
                conn.commit()
                valsD['filX'] = valsD['filY'] = valsD['avgX'] = valsD['avgY'] = valsD['testX'] = valsD['testY'] = 0.00

                # lists routers within range of beacon from closest to farthest
                c.execute('''SELECT DISTINCT 5 router_id FROM network_beacondata WHERE date_time > ? 
                ORDER BY rssi_val DESC''', [datetime.datetime.now()-datetime.timedelta(seconds=1)])
                query = c.fetchall()
                router1 = router2 = router3 = "n/a"
                if len(query) > 5:
                    router1 = query[0]
                    router2 = query[1]
                    router3 = query[2]

                    # Get x-y coordinates for routers
                    c.execute('''SELECT node_location_x, node_location_y FROM network_router WHERE 
                    node_zbMacAddress = ?''', [router1])
                    query = c.fetchall()
                    x_1 = float(query[0]["node_location_x"])
                    x_2 = float(query[0]["node_location_y"])
                    c.execute('''SELECT node_location_x, node_location_y FROM network_router WHERE 
                    node_zbMacAddress = ?''', [router2])
                    query = c.fetchall()
                    y_1 = float(query[0]["node_location_x"])
                    y_2 = float(query[0]["node_location_y"])
                    c.execute('''SELECT node_location_x, node_location_y FROM network_router WHERE 
                    node_zbMacAddress = ?''', [router3])
                    query = c.fetchall()
                    z_1 = float(query[0]["node_location_x"])
                    z_2 = float(query[0]["node_location_y"])

                if inputs[0][-2:] == '27':
                    if addr[-3:] == '242':
                        x_1 = float(inputs[1])
                        x_2 = float(inputs[2])
                    elif addr[-3:] == '223':
                        y_1 = float(inputs[1])
                        y_2 = float(inputs[2])
                    elif addr[-3:] == '948':
                        z_1 = float(inputs[1])
                        z_2 = float(inputs[2])

                    # TODO: make it support multiple, as in more than 3 zigbees
                    # In theory: map each beacon to 3 zigbee device infos, update the buffer

                    if x_1 != float('-inf') and y_1 != float('-inf') and z_1 != float('-inf'):
                        valsD['filX'], valsD['filY'] = triangulate2(x_1, y_1, z_1)
                        valsD['avgX'], valsD['avgY'] = triangulate2(x_2, y_2, z_2)
                        valsD['testX'], valsD['testY'] = biangulate(x_2, y_2)

                        print("Located at: " + str(valsD['filX']) + " , " + str(valsD['filY']))
                        print("Using averages: " + str(valsD['avgX']) + " , " + str(valsD['avgY']))
                        print("Using test: " + str(valsD['testX']) + " , " + str(valsD['testY']))

                        # # Add the thing here?
                        # for item in data:
                        #     with open('../pText/' + item + '.csv', 'a') as f:
                        #         finalData = str(time.time()) + ',' + str(valsD[item]) + '\n'
                        #         fcntl.flock(f, fcntl.LOCK_EX)
                        #         f.write(finalData)
                        #         fcntl.flock(f, fcntl.LOCK_UN)
                        # # add functionality for adding to databases here.
                        # # end

                        if ct > 40:
                            for item in dataD:
                                dataD[item].pop(0)

                        for item in data:
                            dataD[item].append(valsD[item])

                        ct += 1

                # Update x-y values of beacon if it exists, create new entry if it does not.
                # L = [inputs[0]]
                X = '([a-fA-F0-9]{2}[:|\-]?){6}'
                a = re.compile(X).search(inputs[0])
                if a:
                    c.execute('''SELECT beacon_macAddress FROM network_beacon WHERE beacon_macAddress = ?''',
                              [inputs[0]])
                    data1 = c.fetchall()
                    if len(data1) == 0:
                        c.execute('''INSERT INTO network_beacon values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                                  (inputs[0], inputs[0][-2:], 'iBeacon', str(valsD['avgX']), str(valsD['avgY']),
                                   str(valsD['filX']), str(valsD['filY']), str(valsD['testX']), str(valsD['testY']),
                                   router1, router2, router3))
                    else:
                        c.execute('''UPDATE network_beacon SET beacon_avg_location_x = ?, beacon_avg_location_y = ?,
                         beacon_raw_location_x = ?, beacon_raw_location_y = ?, beacon_biangulate_location_x = ?,
                          beacon_biangulate_location_y = ?, beacon_router1_id = ?, beacon_router2_id = ?,
                          beacon_router3_id = ? WHERE beacon_macAddress = ?''',
                                  (str(valsD['avgX']), str(valsD['avgY']), str(valsD['filX']), str(valsD['filY']),
                                   str(valsD['testX']), str(valsD['testY']), router1, router2, router3, inputs[0]))
                conn.commit()

    except InvalidPacketException:
        print("Invalid Checksum??")
        pass

    except KeyboardInterrupt:
        if device is not None and device.is_open():
            device.close()
        c.close()
        conn.close()
    finally:
        if device is not None and device.is_open():
            device.close()
        c.close()
        conn.close()


if __name__ == '__main__':
    main()
